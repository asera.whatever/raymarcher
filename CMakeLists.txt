include_directories(Inc
        Inc/Object)

add_executable(raymarcher
        Src/Main.cpp
        Src/Window.cpp
        Src/Raycaster.cpp
        Src/Framebuffer.cpp)


target_link_libraries(raymarcher sfml-window sfml-graphics sfml-system)