//
// Created by thibault on 14/02/19.
//

#pragma once

#include <SFML/Graphics.hpp>
#include <Framebuffer.hpp>

class Window {
private:
    sf::RenderWindow _window;
public:
    Window(unsigned int width, unsigned int height, const std::string &name);
    ~Window() = default;
    void update();
    void draw(Framebuffer &fb);
    bool isActive();
};