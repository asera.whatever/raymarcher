//
// Created by thibault on 14/02/19.
//

#pragma once

class IObject {
private:
public:
    IObject() = default;
    virtual ~IObject() = default;

    virtual int distanceEstimator() = 0;
};