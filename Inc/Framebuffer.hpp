//
// Created by thibault on 14/02/19.
//

#pragma once

#include <SFML/Graphics.hpp>

class Framebuffer {
private:
    sf::Uint8 *_pixels;
    sf::Texture _texture;
    unsigned int _width;
    unsigned int _height;


public:
    Framebuffer(unsigned int x, unsigned int y);
    ~Framebuffer();

    void putPixel(unsigned int x, unsigned int y, sf::Color color);

    const sf::Uint8 *getPixels() const;
    const sf::Texture &getTexture();

    unsigned int getWidth() const;
    unsigned int getHeight() const;
};
