//
// Created by thibault on 14/02/19.
//

#pragma once

#include <SFML/System/Vector3.hpp>
#include <SFML/Graphics/Color.hpp>

class RayCaster {
private:
    unsigned int _width;
    unsigned int _height;

    unsigned int _screenDist;
    sf::Vector3f _origin;
public:
    RayCaster(sf::Vector3f origin, unsigned int width, unsigned int height, unsigned int _screenDist);
    ~RayCaster() = default;

    sf::Color raycastAt(unsigned int x, unsigned int y, void *scene);
};