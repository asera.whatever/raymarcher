//
// Created by thibault on 14/02/19.
//

#include "Window.hpp"

Window::Window(unsigned int width, unsigned int height, const std::string &name) :
_window(sf::VideoMode(width, height), name, sf::Style::Resize | sf::Style::Close)
{
}

bool Window::isActive() {
    return _window.isOpen();
}

void Window::update() {
    sf::Event event;

    while (_window.pollEvent(event))
        if (event.type == sf::Event::Closed)
            _window.close();
    _window.display();
}


void Window::draw(Framebuffer &fb)
{
    sf::Sprite sprite;

    sprite.setTexture(fb.getTexture(), true);
    _window.draw(sprite);
    _window.display();
}