//
// Created by thibault on 14/02/19.
//

#include <RayCaster.hpp>
#include <iostream>

#include "RayCaster.hpp"

RayCaster::RayCaster(sf::Vector3f origin, unsigned int width, unsigned int height, unsigned int _screenDist) :
_origin(origin),
_width(width),
_height(height),
_screenDist(_screenDist)
{}

sf::Color RayCaster::raycastAt(unsigned int x, unsigned int y, void *scene)
{
    sf::Vector3f screenPoint(_origin.x + 100, _origin.y + y - _height / 2, _origin.z + x - _width / 2);
    sf::Vector3f ray = screenPoint - _origin;


    std::cout << "ray:" << ray.x << ", " << ray.y << ", " << ray.z << std::endl;
    return sf::Color();
}
