//
// Created by thibault on 14/02/19.
//

#include "Framebuffer.hpp"

Framebuffer::Framebuffer(unsigned int width, unsigned int height) :
_width(width),
_height(height),
_texture()
{
    _pixels = new sf::Uint8[width * height * 4];
    _texture.create(width, height);
}

Framebuffer::~Framebuffer() {
    delete _pixels;
}

const sf::Uint8* Framebuffer::getPixels() const {
    return _pixels;
}

void Framebuffer::putPixel(unsigned int x, unsigned int y, sf::Color color) {
    if (x < _width && y < _width) {
        _pixels[(_width * y + x) * 4 + 0] = color.r;
        _pixels[(_width * y + x) * 4 + 1] = color.g;
        _pixels[(_width * y + x) * 4 + 2] = color.b;
        _pixels[(_width * y + x) * 4 + 3] = color.a;
    }
}

const sf::Texture& Framebuffer::getTexture() {
    _texture.update(_pixels, _width, _height, 0, 0);
    return _texture;
}

unsigned int Framebuffer::getHeight() const {
    return _height;
}

unsigned int Framebuffer::getWidth() const {
    return _width;
}