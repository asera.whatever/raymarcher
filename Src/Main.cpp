//
// Created by thibault on 14/02/19.
//

#include <iostream>
#include "Window.hpp"
#include "RayCaster.hpp"

int main(int argc, char **argv)
{
    Window window(400, 400, "raymarcher");
    Framebuffer fb(400, 400);
    sf::Vector3f origin(-100, 0, 0);

    RayCaster rc(origin, 400, 400, 100);

    fb.putPixel(200, 200, sf::Color::Red);

    for (int x = 0; x < 400; x++) {
        for (int y = 0; y < 400; y++) {
            rc.raycastAt(x, y, NULL);
        }
    }
    return 0;
}